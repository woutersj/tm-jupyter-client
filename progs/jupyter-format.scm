(texmacs-module (jupyter-format))

;; To enable copy-paste we need a converter from jupyter-snippet to texmacs
;;
;; Is there a simpler solution re-using other converters?

(define-format jupyter
  (:name "Jupyter source code")
  (:hidden))
  
(define (texmacs->jupyter x . opts)
  (texmacs->verbatim x (acons "texmacs->verbatim:encoding" "SourceCode" '())))

(define (jupyter->texmacs x . opts)
  (code->texmacs x))

(define (jupyter-snippet->texmacs x . opts)
  (code-snippet->texmacs x))

(converter texmacs-tree jupyter-document
  (:function texmacs->jupyter))

(converter jupyter-document texmacs-tree
  (:function jupyter->texmacs))
  
(converter texmacs-tree jupyter-snippet
  (:function texmacs->jupyter))

(converter jupyter-snippet texmacs-tree
  (:function jupyter-snippet->texmacs))
